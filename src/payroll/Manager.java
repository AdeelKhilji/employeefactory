/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 * Class Manager extends Employee
 * @author Adeel Khilji
 */
public class Manager extends Employee
{
    private double bonus;//Instance variable bonus

    protected Manager(String name, double hourlyWage, int numOfHours, double bonus)
    {
        super(name, hourlyWage, numOfHours);
        this.bonus = bonus;
    }
    
    public String getName()
    {
        return super.getName();
    }
    public double getBonus()
    {
        return this.bonus;
    }
    /**
     * calculatePay() - calculate method
     * @return double
     */
    @Override
    public double calculatePay()
    {
        return super.calculatePay() + bonus;
    }
}
