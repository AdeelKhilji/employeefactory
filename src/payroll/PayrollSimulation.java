/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 * Class PayrollSimulation
 * @author Adeel Khilji
 */
public class PayrollSimulation 
{
    /**
     * 
     * @param args String 
     */
    public static void main(String[] args)
    {   
        EmployeeFactory factory = EmployeeFactory.getInstance();
        
        Employee oneEmployee = new Employee("Jane",14.50,44);
        
        Employee employee = factory.getEmployee(EmployeeType.EMPLOYEE);
        
        String employeeFormat = " NAME: " + oneEmployee.getName() + " TOTAL PAY: " + employee.calculatePay();
        
        Manager oneManager = new Manager("Reh mat ali",14.50,44, 20.00);
        
        employee = factory.getEmployee(EmployeeType.MANAGER);
        
        String managerFormat = " NAME: " + oneManager.getName() + " TOTAL PAY: " + employee.calculatePay();
        
        System.out.println("EMPLOYEE " + employeeFormat.toString());
        System.out.println("MANAGER " + managerFormat.toString()); 
    }
}
