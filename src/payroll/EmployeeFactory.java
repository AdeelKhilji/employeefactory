/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payroll;

/**
 *
 * @author Adeel Khilji
 */
public class EmployeeFactory 
{
    private static EmployeeFactory employeeFactory;
    
    private EmployeeFactory(){}
    
    public static EmployeeFactory getInstance()
    {
        if(employeeFactory == null)
        {
            employeeFactory = new EmployeeFactory();
        }
        return employeeFactory;
    }
    
    public Employee getEmployee(EmployeeType type)
    {
        Employee employee = null;
        switch(type)
        {
            case EMPLOYEE: employee = new Employee("John",14.50,44);
            break;
            case MANAGER: employee = new Manager("Sam", 15.50,46,10.00);
            break;
        }
        return employee;
    }
}
